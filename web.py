#!/usr/bin/env python

import getopt
import os
import sys

from BaseHTTPServer import HTTPServer

from webserver import staticfiles

def die(message=None):
    if message:
        print >> sys.stderr, message
    else:
        print >> sys.stderr, """Very simple static file webserver.

Will serve the contents of pwd. To serve a different directory, supply a
document root.

Usage: %s OPTIONS [document root]

OPTIONS
    -p, --port      TCP port to listen on (default 8008)
""" % sys.argv[0]
    exit(1)

if __name__ == '__main__':

    port = 8008
    root = os.getcwd()

    options, remainder = getopt.getopt(sys.argv[1:], 'p:', ['port='])
    for opt, arg in options:
        if opt in ('-p', '--port'):
            try:
                port = int(arg)
            except ValueError:
                die('Error: invalid port number: %s' % arg)
        else:
            die()
    if len(remainder) > 1:
        die()
    elif len(remainder) == 1:
        if not os.path.exists(remainder[0]):
            die('Error: %s not found.' % remainder[0])
        else:
            root = remainder[0]

    httpd = HTTPServer(('', port), staticfiles.handler(root))
    print 'Serving %s on port %s...' % (root, port)
    httpd.serve_forever()
