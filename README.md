Very simple line-per-second counter, visualized through a js speed gauge.

![screenshot](https://bitbucket.org/evzijst/speedometer/raw/master/screenshot.png)

