from datetime import datetime
import errno
import os
from stat import S_ISDIR, S_ISREG, S_ISLNK, ST_MODE, ST_MTIME, ST_SIZE
import traceback

from BaseHTTPServer import BaseHTTPRequestHandler

from webserver.mimetypes import mimetypes
from webserver import templates

class NotFound(Exception):
    pass

def handler(root=None):
    root = os.path.normpath(root or os.getcwd())

    class StaticFileHandler(BaseHTTPRequestHandler):

        def _contenttype(self, path):
            return mimetypes.get(path.split('.')[-1].lower(), 'text/plain')

        def _serve404(self):
            html = u"""
            <html>
                <head>
                    <title>Document not found</title>
                </head>
                <body>
                    <h2>Document not found (404)</h2>
                </body>
            </html>
            """.encode('utf-8')
            self.send_response(404, message='Not found')
            self.send_header('Content-type', 'text/html; charset=utf-8')
            self.send_header('Content-length', len(html))
            self.end_headers()
            self.wfile.write(html)

        def _serve500(self, exception=None):
            tb = traceback.format_exc().encode('utf-8')
            self.send_response(500)
            self.send_header('Content-type',
                             'text/plain; charset=utf-8')
            self.send_header('Content-length', len(tb))
            self.end_headers()
            self.wfile.write(tb)

        def _servefile(self, path):
            try:
                with open(path) as _file:
                    self.send_response(200)
                    self.send_header('Content-type', self._contenttype(path))
                    self.send_header('Content-length', os.path.getsize(path))
                    self.end_headers()
                    while True:
                        buf = _file.read(4096)
                        if buf:
                            self.wfile.write(buf)
                        else:
                            break
            except IOError, e:
                if e.errno == errno.ENOENT:
                    self._serve404()
                else:
                    self._serve500(exception=e)

        def _servedir(self, dir):

            rel = os.path.relpath(dir, root)
            rel = '' if rel == '.' else rel

            listing = []
            head, tail = os.path.split(rel)
            if tail:
                listing.append('<tr><td colspan=3><a href="/%s">..</a></td></tr>' % head)

            dirs, files = [], []
            for f in os.listdir(dir):
                _stat = os.lstat(os.path.join(dir, f))
                if S_ISDIR(_stat[ST_MODE]):
                    dirs.append((f, _stat))
                if S_ISREG(_stat[ST_MODE]) or S_ISLNK(_stat[ST_MODE]):
                    files.append((f, _stat))

            entry = (lambda name, type, size, mtime: (
                    '<tr>'
                    '<td><a class="icon {type}" href="/{path}">{name}</a></td>'
                    '<td class="detailsColumn">{size}</td>'
                    '<td class="detailsColumn">{mtime}</td>'
                    '</tr>'.format(path=os.path.join(rel, name), type=type,
                            name=name, size=size,
                            mtime=datetime.fromtimestamp(mtime))))

            for d, _stat in dirs:
                listing.append(entry(d, 'dir', '', _stat[ST_MTIME]))
            for f, _stat in files:
                listing.append(entry(f, 'file', _stat[ST_SIZE], _stat[ST_MTIME]))

            html = templates.dir % {'path': rel, 'listing': '\n'.join(listing)}

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.send_header('Content-length', len(html))
            self.end_headers()
            self.wfile.write(html)

        def do_GET(self):

            path = self.path.split('?', 1)[0]   # remove query string
            path = os.path.normpath('/' + path.lstrip('/'))
            path = os.path.join(root, path.lstrip('/'))

            if os.path.isdir(path):
                index = os.path.join(path, 'index.html')
                if os.path.isfile(index):
                    self._servefile(index)
                else:
                    self._servedir(path)
            elif os.path.isfile(path):
                self._servefile(path)
            else:
                self._serve404()

    return StaticFileHandler
