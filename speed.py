#!/usr/bin/env python
#
# Simple script that measures number of lines per second on stdin and writes
# (not appends) the result to a file.

import getopt
import os
import select
import sys
import time

def die(message=None, ret=-1):
    if message:
        print >> sys.stderr, message
    else:
        print >> sys.stderr, """\
Measures number of lines per second on stdin and either writes (not appends)
the result to a file, or displays it on stdout.

Usage: %s OPTIONS [output]

OPTIONS
    -o, --output    write output to file (use - or omit to write to stdout)
    -h, --help      print this help and exit
""" % sys.argv[0]
    exit(ret)

options, remainder = getopt.getopt(sys.argv[1:], 'o:h', ['output', 'help'])
output = None

for opt, arg in options:
    if opt in ('-o', '--output') and arg != '-':
        output = arg
    elif opt in ('-h', '--help'):
        die(ret=0)
if remainder:
    die()

out = open(output, 'w') if output else sys.stdout
try:
    stdin = sys.stdin.fileno()
    start = time.time()
    count = 0
    while True:
        i, o, e = select.select([stdin], [], [], 1)
        if stdin in i:
            buf = os.read(stdin, select.PIPE_BUF)
            if not buf:
                break
            else:
                count += buf.count('\n')
        now = time.time()
        if now - start > 1.0:
            start += 1.0
            out.write(str(count))
            if out == sys.stdout:
                out.write('\r')
            else:
                out.truncate(len(str(count)))
                out.seek(0)
            out.flush()
            count = 0
finally:
    if out != sys.stdout:
        out.close()
